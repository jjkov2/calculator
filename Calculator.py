inp = input()
expression = ""
result = 0
for i in range(0, len(inp)):
    if inp[i].isdigit() == False:
        expression += f" {inp[i]} "
    else:
        expression += inp[i]
expression = expression.split()
for i in range(0, len(expression)-1):
    if expression[i] == "*":
        result += int(expression[i-1]) * int(expression[i+1])
    elif expression[i] == "/":
        result = result / int(expression[i])
    elif expression[i] == "+":
        result += int(expression[i-1])
    elif expression[i] == "-":
        result -= int(expression[i])
print(result)